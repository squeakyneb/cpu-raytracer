#pragma once

#include <glm/glm.hpp>
#include <memory>
#include "MaterialBase.h"
#include "Ray.h"


class Hit
{
public:
	Hit(double distance, glm::dvec3 position, glm::dvec3 incident, glm::dvec3 normal, MaterialBase* material);
	~Hit();

	Ray next_ray() const;
	void gather_light(glm::dvec3& light) const;

	double distance;
	glm::dvec3 position;
	glm::dvec3 incident;
	glm::dvec3 nextDir;
	glm::dvec3 normal;
	MaterialBase* material;
};

