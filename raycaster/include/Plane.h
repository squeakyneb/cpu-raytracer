#pragma once

#include "ObjectBase.h"

class Plane :
	public ObjectBase
{
public:
	Plane(glm::dvec3 centre, glm::dvec3 normal);
	~Plane();

	std::optional <Hit> intersect_plane(Ray ray) const;
	virtual std::optional<Hit> intersect(Ray ray) const;

	glm::dvec3 centre;
	glm::dvec3 normal;
	std::unique_ptr<MaterialBase> material;
};

