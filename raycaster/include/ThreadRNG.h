#pragma once

#include <random>
#include <algorithm>
#include <omp.h>

class ThreadRNG
{
public:
	typedef std::mt19937 RNGENGINE;

	static ThreadRNG& getInstance();
	double dRand();
	double dRand(double min, double max);

	// delete copy and move constructors and assign operators
	ThreadRNG(ThreadRNG const&) = delete;             // Copy construct
	ThreadRNG(ThreadRNG&&) = delete;                  // Move construct
	ThreadRNG& operator=(ThreadRNG const&) = delete;  // Copy assign
	ThreadRNG& operator=(ThreadRNG&&) = delete;       // Move assign

protected:
	ThreadRNG();
	~ThreadRNG();

	std::vector<RNGENGINE> engines;
	std::uniform_real_distribution<double> zeroToOne;
};