#pragma once
#include <vector>
#include <memory>
#include "ObjectBase.h"
#include "Hit.h"

class Scene
{
public:
	Scene();
	~Scene();

	std::optional<Hit> trace_scene(Ray ray) const;

	std::vector<std::unique_ptr<ObjectBase>> objects;
};

