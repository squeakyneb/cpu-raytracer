#pragma once
#include "Ray.h"
#include "Hit.h"
#include <optional>

class ObjectBase
{
public:
	virtual ~ObjectBase() = 0;

	virtual std::optional<Hit> intersect(Ray ray) const = 0;
};

