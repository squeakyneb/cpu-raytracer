#pragma once
#include "Plane.h"

class Disc :
	public Plane
{
public:
	Disc(glm::dvec3 centre, glm::dvec3 normal, double radius);
	~Disc();

	std::optional<Hit> intersect(Ray ray) const;

	double radius, radius_sqr;
};

