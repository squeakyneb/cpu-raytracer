#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include "MaterialBase.h"
#include "ThreadRNG.h"
#include "Hit.h"

class DiffuseMaterial : public MaterialBase
{
public:
	DiffuseMaterial();
	DiffuseMaterial(glm::dvec3 d, glm::dvec3 e);
	~DiffuseMaterial();

	void gather_light(glm::dvec3& light, const Hit& hit) const;
	glm::dvec3 reflect(const Hit& hit) const;

	glm::dvec3 diffuse;
	glm::dvec3 emissive;
};

