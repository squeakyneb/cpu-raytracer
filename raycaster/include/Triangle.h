#pragma once
#include "Plane.h"

class Triangle :
	public Plane
{
public:
	Triangle(glm::dvec3 a, glm::dvec3 b, glm::dvec3 c);
	~Triangle();

	std::optional<Hit> intersect(Ray ray) const;

	glm::dvec3 b, c;
};

