#pragma once

#include <glm/glm.hpp>

class Ray
{
public:
	Ray(glm::dvec3 origin, glm::dvec3 direction);
	~Ray();

	glm::dvec3 getOrigin();
	void setOrigin(glm::dvec3 origin);
	glm::dvec3 getDirection();
	void setDirection(glm::dvec3 Direction);

private:
	glm::dvec3 rayOrigin;
	glm::dvec3 rayDirection;
};

