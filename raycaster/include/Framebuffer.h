#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <algorithm>
#include "Image.h"

class Framebuffer
{
public:
	Framebuffer(int width, int height);
	~Framebuffer();

	int convert_address(int x, int y);
	void set_pixel(glm::dvec4 c, int x, int y);
	void accumulate_pixel(glm::dvec4 c, int x, int y);
	glm::dvec4 get_pixel(int x, int y);
	void save_tga(std::string filename);

private:
	std::vector<glm::dvec4> pixels;
	int bufwidth;
	int bufheight;
};

