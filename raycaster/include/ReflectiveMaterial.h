#pragma once

#include <glm/glm.hpp>
#include "MaterialBase.h"
#include "Hit.h"

class ReflectiveMaterial : public MaterialBase
{
public:
	ReflectiveMaterial();
	~ReflectiveMaterial();

	void gather_light(glm::dvec3& light, const Hit& hit) const;
	glm::dvec3 reflect(const Hit& hit) const;

};

