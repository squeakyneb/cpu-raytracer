#pragma once
#include <glm/glm.hpp>

class Hit; //forward dec of Hit.h

class MaterialBase
{
public:
	virtual ~MaterialBase() = 0;

	virtual void gather_light(glm::dvec3& light, const Hit& hit) const = 0;
	virtual glm::dvec3 reflect(const Hit& hit) const = 0;
};

