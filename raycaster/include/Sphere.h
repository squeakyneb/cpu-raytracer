#pragma once

#include <glm/glm.hpp>
#include <utility>
#include "ObjectBase.h"
#include "Ray.h"
#include "Hit.h"
#include "MaterialBase.h"

class Sphere : public ObjectBase
{
public:
	Sphere(glm::dvec3 centre, double radius);
	~Sphere();

	std::optional<Hit> intersect(Ray ray) const;

	glm::dvec3 centre;
	double radius;
	std::unique_ptr<MaterialBase> material;
};

