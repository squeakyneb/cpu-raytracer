// (c) 2015 Benny Mackney
// Licensed under the MIT License (see README.md)

#include <iostream>
#include <cstdio>
#include "argtable3.h"
#include <chrono>
#include <memory>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "Scene.h"
#include "Ray.h"
#include "Hit.h"
#include "Framebuffer.h"
#include "Materials.h"
#include "Objects.h"
#include "ThreadRNG.h"

const int MAX_BOUNCE = 3;

glm::dvec4 getpix(short x, short y, short w, short h, Scene& scene) {
	double rayPixX = double(x);
	double rayPixY = double(y);

	ThreadRNG& rng = ThreadRNG::getInstance();
	rayPixX += rng.dRand(-0.5, 0.5);
	rayPixY += rng.dRand(-0.5, 0.5);

	glm::dvec2 uv = glm::dvec2(rayPixX / double(w), rayPixY / double(h)) * 2.0 - 1.0; // -1..+1 screenspace
	uv.x *= (double(w) / double(h));

	glm::dvec3 camForward = glm::dvec3(0.f, 1.f, 0.f);
	glm::dvec3 camUp = glm::dvec3(0.f, 0.f, 1.f);
	glm::dvec3 camRight = glm::cross(camForward, camUp);
	Ray cameraRay = Ray(glm::dvec3(0.f, -2.f, 1.f), camForward + camUp * uv.y + camRight * uv.x);

	auto cameraRayHit = scene.trace_scene(cameraRay);

	if (!cameraRayHit)
	{
		return glm::dvec4(0.);
	}


	std::vector<Hit> hits;
	hits.reserve(MAX_BOUNCE);
	hits.push_back(cameraRayHit.value());

	Ray nextRay = hits[0].next_ray();

	for (int bounces = 0; bounces < MAX_BOUNCE; bounces++)
	{
		auto newHit = scene.trace_scene(nextRay);
		if (!newHit) {
			break;
		}
		hits.push_back(newHit.value());
		nextRay = hits.back().next_ray();
	}

	glm::dvec3 light = glm::dvec3(0.);
	for (auto bounce = hits.rbegin(); bounce != hits.rend(); bounce++) {
		bounce->gather_light(light);
	}

	glm::dvec4 returnCol;
	returnCol.r = light.r;
	returnCol.g = light.g;
	returnCol.b = light.b;
	returnCol.a = 1.0;
	return returnCol;
}

struct arg_lit* help;
struct arg_int* a_width, * a_height;
struct arg_int* a_sampmax, * a_sampmin;
struct arg_int* a_tmax, * a_tmin;
struct arg_file* a_output;
struct arg_end* a_end;

int main(int argc, char* argv[])
{
	void* argtable[] = {
		help = arg_litn("h", "help", 0, 1, "Display help and exit"),
		a_sampmin = arg_intn("s", "sampmin", "<n>",0,1,"Minimum number of samples to take"),
		a_sampmax = arg_intn("S", "sampmax", "<n>",0,1,"Maximum number of samples to take"),
		a_tmin = arg_intn("t", "tmin", "<n>",0,1,"Minimum number of seconds to run for"),
		a_tmax = arg_intn("T", "tmax", "<n>",0,1,"Maximum number of seconds to run for"),
		a_width = arg_intn(NULL, NULL, "<width>",1,1,"Width of the output image"),
		a_height = arg_intn(NULL, NULL, "<height>",1,1,"Height of the output image"),
		a_output = arg_filen(NULL, NULL, "<output>",1,1,"Filename for output image"),
		a_end = arg_end(10)
	};

	int nerrors = arg_parse(argc, argv, argtable);

	if (help->count > 0)
	{
		std::cout << "Usage: " << argv[0] << " ";
		arg_print_syntax(stdout, argtable, "\n");
		std::cout << "Raytracer" << std::endl;
		arg_print_glossary(stdout, argtable, "%-25s %s\n");
		return 0;
	}
	if (nerrors > 0)
	{
		arg_print_errors(stdout, a_end, argv[0]);
		return -1;
	}

	// Width and height
	unsigned short width;
	unsigned short height;
	if (*(a_width->ival) > 65535 ||
		*(a_height->ival) > 65535)
	{
		std::cout << "No output image format currently used supports dimensions greater than 65535. Anyway, that's ridiculous." << std::endl;
		return -2;
	}
	else if (*(a_width->ival) < 1 ||
		*(a_height->ival) < 1)
	{
		std::cout << "Image dimensions must be positive." << std::endl;
		return -3;
	}
	width = *(a_width->ival);
	height = *(a_height->ival);


	// Sample limits
	int MIN_SAMPLES = 0;
	int MAX_SAMPLES = 0;
	if (a_sampmax->count)
	{
		if (*(a_sampmax->ival) < 1)
		{
			std::cout << "Sample maximum must be at least one sample." << std::endl;
			return -4;
		}
		MAX_SAMPLES = *(a_sampmax->ival);
	}
	if (a_sampmin->count)
	{
		MIN_SAMPLES = *(a_sampmin->ival);
	}

	// Time limits
	int MIN_TIME = 0;
	int MAX_TIME = 0;
	if (a_tmax->count)
	{
		if (*(a_tmax->ival) < 1)
		{
			std::cout << "Time maximum must be at least one second." << std::endl;
			return -5;
		}
		MAX_TIME = *(a_tmax->ival);
	}
	if (a_tmin->count)
	{
		MIN_TIME = *(a_tmin->ival);
	}

	if (!(MIN_TIME || MAX_TIME || MIN_SAMPLES || MAX_SAMPLES))
	{
		std::cout << "No limits set - no point starting a job that will never be done" << std::endl;
		return -6;
	}

	string filename = *(a_output->filename);

	// done with the arguments, clean up
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0])); // sizeof(array) is in bytes, divide by the size of a single pointer for a count

	Scene myScene;

	std::unique_ptr<Sphere> tmpSphere;
	// floor
	std::unique_ptr<Plane> tmpPlane;
	tmpPlane.reset(new Plane(glm::dvec3(0.), glm::dvec3(0., 0., 1.)));
	tmpPlane->material.reset(new DiffuseMaterial(glm::dvec3(.7), glm::dvec3(0.)));
	myScene.objects.push_back(std::move(tmpPlane));

	// behold, the sun
	tmpSphere.reset(new Sphere(glm::dvec3(4., 0., 4.), 1.f));
	tmpSphere->material.reset(new DiffuseMaterial(glm::dvec3(0.), glm::dvec3(1.0)));
	myScene.objects.push_back(std::move(tmpSphere));

	// green centre
	tmpSphere.reset(new Sphere(glm::dvec3(0., 0., .5), .5f));
	tmpSphere->material.reset(new DiffuseMaterial(glm::dvec3(0., 1., 0.), glm::dvec3(0.)));
	myScene.objects.push_back(std::move(tmpSphere));

	// red background
	tmpSphere.reset(new Sphere(glm::dvec3(-3., 13., 0.), 10.f));
	tmpSphere->material.reset(new DiffuseMaterial(glm::dvec3(1., 0., 0.), glm::dvec3(0.)));
	myScene.objects.push_back(std::move(tmpSphere));

	// mirror ball
	tmpSphere.reset(new Sphere(glm::dvec3(1., 0., .3), .3f));
	tmpSphere->material.reset(new ReflectiveMaterial());
	myScene.objects.push_back(std::move(tmpSphere));

	// reflector
	std::unique_ptr<Triangle> tmpTriangle;
	glm::dvec3 a = glm::dvec3(-1.5, 1.5, .0);
	glm::dvec3 b = glm::dvec3(-.5, 2., .0);
	glm::dvec3 c = glm::dvec3(-1.5, 1.5, 1.0);
	glm::dvec3 d = glm::dvec3(-.5, 2., 1.0);

	tmpTriangle.reset(new Triangle(a, b, c));
	tmpTriangle->material.reset(new ReflectiveMaterial());
	myScene.objects.push_back(std::move(tmpTriangle));
	tmpTriangle.reset(new Triangle(b, c, d));
	tmpTriangle->material.reset(new ReflectiveMaterial());
	myScene.objects.push_back(std::move(tmpTriangle));

	// mirror ball
	tmpSphere.reset(new Sphere(glm::dvec3(-1., 0., .3), .3f));
	tmpSphere->material.reset(new ReflectiveMaterial());
	myScene.objects.push_back(std::move(tmpSphere));

	// sun blocker
	tmpSphere.reset(new Sphere(glm::dvec3(0, 0, 1.5), .5f));
	tmpSphere->material.reset(new DiffuseMaterial(glm::dvec3(0., 0., 1.), glm::dvec3(0.)));
	myScene.objects.push_back(std::move(tmpSphere));

	Framebuffer buffer(width, height);
#pragma omp parallel
	{
#pragma omp master
		std::cout << "Using " << omp_get_num_threads() << " threads" << std::endl;
	}

	std::cout << "Rendering " << width << "x" << height << " to " << filename << std::endl;

	auto startTime = std::chrono::system_clock::now();
	int sample = 0;
	bool RUNNING = true;
	while (RUNNING) {
		sample++;
		// Work
#pragma omp parallel for schedule(dynamic,1)
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++)
			{
				glm::dvec4 c = getpix(x, y, width, height, myScene);
				buffer.accumulate_pixel(c, x, y);
			}
		}
		// Report
		std::cout << "Done sample " << sample << " ";
		if (MIN_SAMPLES && MAX_SAMPLES)
		{
			std::cout << "(min: " << MIN_SAMPLES << ", max: " << MAX_SAMPLES << ") ";
		}
		else if (MIN_SAMPLES)
		{
			std::cout << "(min: " << MIN_SAMPLES << ") ";
		}
		else if (MAX_SAMPLES)
		{
			std::cout << "(max: " << MAX_SAMPLES << ") ";
		}

		auto currTime = std::chrono::system_clock::now();
		std::chrono::duration<double> timeTaken = currTime - startTime;
		std::cout << timeTaken.count() << " seconds ";
		if (MIN_TIME && MAX_TIME)
		{
			std::cout << "(min: " << MIN_TIME << ", max: " << MAX_TIME << ") ";
		}
		else if (MIN_TIME)
		{
			std::cout << "(min: " << MIN_TIME << ") ";
		}
		else if (MAX_TIME)
		{
			std::cout << "(max: " << MAX_TIME << ") ";
		}
		std::cout << std::endl;

		// Exits
		if (MAX_SAMPLES && sample >= MAX_SAMPLES)
		{
			std::cout << "Job ended at maximum sample limit" << std::endl;
			RUNNING = false;
		}
		if (MAX_TIME && timeTaken.count() > MAX_TIME)
		{
			std::cout << "Job ended at maximum time limit" << std::endl;
			RUNNING = false;
		}
		if (MIN_TIME && MIN_SAMPLES)
		{
			if (timeTaken.count() > MIN_TIME && sample >= MIN_SAMPLES) {
				std::cout << "Job ended, satisfied all supplied minimums" << std::endl;
				RUNNING = false;
			}
		}
		else if (MIN_TIME && timeTaken.count() > MIN_TIME) {
			std::cout << "Job ended at minimum time requirement" << std::endl;
			RUNNING = false;
		}
		else if (MIN_SAMPLES && sample >= MIN_SAMPLES) {
			std::cout << "Job ended at minimum sample requirement" << std::endl;
			RUNNING = false;
		}
	}
	auto endTime = std::chrono::system_clock::now();
	std::chrono::duration<double, std::milli> timeTaken = endTime - startTime;
	std::cout << "Rendered in " << timeTaken.count() / 1000.f << " seconds" << std::endl;
	std::cout << "Normalising and saving file" << std::endl;

	buffer.save_tga(filename);

	std::cout << std::endl << "And we're done!" << std::endl;
	return 0;
}
