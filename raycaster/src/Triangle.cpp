#include "Triangle.h"


Triangle::Triangle(glm::dvec3 a, glm::dvec3 b, glm::dvec3 c)
	: Plane(a, glm::cross(b - a, c - a)), b(b), c(c)
{
}

Triangle::~Triangle()
{
}

std::optional<Hit> Triangle::intersect(Ray ray) const
{
	auto plane_hit = intersect_plane(ray);

	if (!plane_hit) {
		return {};
	}
	double u, v; // barycentric coordinates

	glm::dvec3 check;

	glm::dvec3 edge0 = b - centre;
	glm::dvec3 vp0 = plane_hit->position - centre;
	check = glm::cross(edge0, vp0);
	if (glm::dot(normal, check) < 0.) {
		return {};
	}
	glm::dvec3 edge1 = c - b;
	glm::dvec3 vp1 = plane_hit->position - b;
	check = glm::cross(edge1, vp1);
	if (u = glm::dot(normal, check) < 0.) {
		return {};
	}
	glm::dvec3 edge2 = centre - c;
	glm::dvec3 vp2 = plane_hit->position - c;
	check = glm::cross(edge2, vp2);
	if ((v = glm::dot(normal, check)) < 0.) {
		return {};
	}

	return plane_hit;
}
