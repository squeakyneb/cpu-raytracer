#include "Scene.h"



Scene::Scene()
{
}


Scene::~Scene()
{
}

std::optional<Hit> Scene::trace_scene(Ray ray) const
{
	double closest = INFINITY;
	std::optional<Hit> hit;
	for (auto& obj : objects)
	{
		auto nextHit = obj->intersect(ray);
		if (nextHit // did hit
			&& (nextHit->distance < closest) // isn't occluded
			// TODO:  replace the following line with some ID system
			&& (nextHit->distance > 0.00001) // isn't the object we just left
			)
		{
			closest = nextHit->distance;
			hit = nextHit;
		}
	}
	return hit;
}
