#include "Ray.h"



Ray::Ray(glm::dvec3 origin, glm::dvec3 direction)
{
	setOrigin(origin);
	setDirection(direction);
}


Ray::~Ray()
{
}


glm::dvec3 Ray::getOrigin()
{
	return rayOrigin;
}

void Ray::setOrigin(glm::dvec3 origin)
{
	rayOrigin = origin;
}

glm::dvec3 Ray::getDirection()
{
	return rayDirection;
}

void Ray::setDirection(glm::dvec3 direction) {
	rayDirection = glm::normalize(direction);
}