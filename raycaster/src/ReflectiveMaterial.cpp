#include "ReflectiveMaterial.h"

ReflectiveMaterial::ReflectiveMaterial()
{
}


ReflectiveMaterial::~ReflectiveMaterial()
{
}

void ReflectiveMaterial::gather_light(glm::dvec3& light, const Hit& hit) const
{
}

glm::dvec3 ReflectiveMaterial::reflect(const Hit& hit) const
{
	glm::dvec3 nextDir = glm::reflect(hit.incident, hit.normal);
	return nextDir;
}
