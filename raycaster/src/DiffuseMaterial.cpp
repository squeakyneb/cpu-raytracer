#include "DiffuseMaterial.h"

DiffuseMaterial::DiffuseMaterial()
	: diffuse(glm::dvec3(.5)), emissive(glm::dvec3(0.))
{
}

DiffuseMaterial::DiffuseMaterial(glm::dvec3 d, glm::dvec3 e)
	:diffuse(d), emissive(e)
{
}


DiffuseMaterial::~DiffuseMaterial()
{
}

void DiffuseMaterial::gather_light(glm::dvec3& light, const Hit& hit) const
{
	light = light * diffuse * glm::dot(hit.normal, hit.nextDir) + emissive;
}

glm::dvec3 DiffuseMaterial::reflect(const Hit& hit) const
{
	ThreadRNG& rng = ThreadRNG::getInstance();
	double phi = rng.dRand(0.f, glm::pi<double>() * 2.);
	double costheta = rng.dRand(-1., 1.);
	double theta = glm::acos(costheta);
	glm::dvec3 nextDir = glm::dvec3(
		glm::sin(theta) * glm::cos(phi),
		glm::sin(theta) * glm::sin(phi),
		glm::cos(theta)
	);
	if (glm::dot(nextDir, hit.normal) < 0) // the ray is going inside
	{
		nextDir = -nextDir; // flip it back out
	}
	return nextDir;
}
