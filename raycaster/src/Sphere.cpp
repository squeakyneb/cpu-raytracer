#include "Sphere.h"


Sphere::Sphere(glm::dvec3 centrePosition, double sphereRadius)
	: centre(centrePosition), radius(sphereRadius)
{
}


Sphere::~Sphere()
{
}

std::optional<Hit> Sphere::intersect(Ray ray) const
{
	const glm::dvec3 ro = ray.getOrigin();
	const glm::dvec3 rd = ray.getDirection();

	glm::dvec3 centreOffset = ro - centre;
	double squaredDist = glm::dot(centreOffset, centreOffset);

	// quadratic black magic

	double a = glm::dot(rd, rd);
	double b = 2.f * glm::dot(rd, ro - centre);
	double c = glm::dot(ro - centre, ro - centre) - (radius * radius);

	// discriminant
	double disc = (b * b) - (4.f * a * c);
	if (disc < 0.f)
	{
		return {};
	}

	double sqrtDisc = glm::sqrt(disc);
	double t0 = (-b - sqrtDisc) / (2.0 * a);
	double t1 = (-b + sqrtDisc) / (2.0 * a);

	if (t1 < 0.) // the "far side" is behind us
	{
		return {};
	}
	else if (t0 < 0.) // far side in front but near side behind (inside)
	{
		// we're inside
		t0 = t1;
	}
	auto position = ro + rd * double(t0);
	auto normal = glm::normalize(position - centre);
	return Hit(t0, position, rd, normal, material.get());
}