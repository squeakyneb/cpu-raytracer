#include "Hit.h"

Hit::Hit(double _distance, glm::dvec3 _position, glm::dvec3 _incident, glm::dvec3 _normal, MaterialBase* _material)
	: distance(_distance), position(_position), incident(_incident), normal(_normal), material(_material)
{
	nextDir = material->reflect(*this);
}


Hit::~Hit()
{
}

Ray Hit::next_ray() const
{
	return Ray(position, nextDir);
}

void Hit::gather_light(glm::dvec3& light) const
{
	material->gather_light(light, *this);
}
