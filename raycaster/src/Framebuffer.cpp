#include "Framebuffer.h"


Framebuffer::Framebuffer(int width, int height)
{
	bufwidth = width;
	bufheight = height;
	pixels.resize(bufwidth * bufheight, glm::dvec4(0.));
}

Framebuffer::~Framebuffer()
{
}

int Framebuffer::convert_address(int x, int y)
{
	return bufwidth * y + x;
}

void Framebuffer::set_pixel(glm::dvec4 c, int x, int y)
{
	pixels[convert_address(x, y)] = c;
}

void Framebuffer::accumulate_pixel(glm::dvec4 c, int x, int y)
{
	set_pixel(get_pixel(x, y) + c, x, y);
}

glm::dvec4 Framebuffer::get_pixel(int x, int y)
{
	return pixels[convert_address(x, y)];
}

void Framebuffer::save_tga(std::string filename)
{
	double accumulate = 0.;
	for (int x = 0; x < bufwidth; x++) {
		for (int y = 0; y < bufheight; y++)
		{
			glm::dvec4 pixel = get_pixel(x, y);
			if (pixel.a > 0.01) // ignore empty space
			{
				accumulate += pixel.r;
				accumulate += pixel.g;
				accumulate += pixel.b;
			}
		}
	}

	double mean = accumulate / (bufwidth * bufheight * 3);

	accumulate = 0.;
	for (int x = 0; x < bufwidth; x++) {
		for (int y = 0; y < bufheight; y++)
		{
			glm::dvec4 pixel = get_pixel(x, y);
			if (pixel.a > 0.01) // ignore empty space
			{
				accumulate += glm::pow(pixel.r - mean, 2.0);
				accumulate += glm::pow(pixel.g - mean, 2.0);
				accumulate += glm::pow(pixel.b - mean, 2.0);
			}
		}
	}

	double mean_deviation = glm::sqrt(accumulate / (bufwidth * bufheight * 3));
	int skipped_due_to_dev = 0;

	double maxSample = 0.;
	double minSample = INFINITY;
	for (int x = 0; x < bufwidth; x++) {
		for (int y = 0; y < bufheight; y++)
		{
			glm::dvec4 pixel = get_pixel(x, y);
			double lum = (pixel.r + pixel.g + pixel.b) / 3.;
			double deviation = abs(lum - mean);
			if (deviation > mean_deviation * 2.0)
			{
				skipped_due_to_dev++;
			}
			else if (pixel.a > 0.01) // ignore empty space
			{
				maxSample = max(maxSample, pixel.r);
				maxSample = max(maxSample, pixel.g);
				maxSample = max(maxSample, pixel.b);
				minSample = min(minSample, pixel.r);
				minSample = min(minSample, pixel.g);
				minSample = min(minSample, pixel.b);
			}
		}
	}
	double dynamicRange = maxSample - minSample;


	TGAImage* img = new TGAImage(bufwidth, bufheight);

#pragma omp parallel for
	for (int x = 0; x < bufwidth; x++) {
		for (int y = 0; y < bufheight; y++)
		{
			glm::dvec4 c = get_pixel(x, y);
			double alpha = c.a; // preserve alpha
			c = (c - minSample) / dynamicRange;
			c.a = alpha;
			Colour pixel;
			pixel.r = glm::clamp(int(c.r * 255.), 0, 255);
			pixel.g = glm::clamp(int(c.g * 255.), 0, 255);
			pixel.b = glm::clamp(int(c.b * 255.), 0, 255);
			pixel.a = glm::clamp(int(c.a * 255.), 0, 255);
			img->setPixel(pixel, x, y);
		}
	}
	//write the image to disk
	img->WriteImage(filename);

	delete img;
}
