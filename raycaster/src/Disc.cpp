#include "..\include\Disc.h"



Disc::Disc(glm::dvec3 centre, glm::dvec3 normal, double radius)
	: Plane(centre, normal), radius(radius), radius_sqr(radius* radius)
{
}

Disc::~Disc()
{
}

std::optional<Hit> Disc::intersect(Ray ray) const
{
	auto plane_hit = intersect_plane(ray);
	if (plane_hit) {
		glm::dvec3 v = plane_hit->position - centre;
		double dist_sqr = glm::dot(v, v);
		if (dist_sqr < radius_sqr) {
			return plane_hit;
		}
	}
	return {};
}
