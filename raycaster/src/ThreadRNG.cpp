#include "ThreadRNG.h"



ThreadRNG::ThreadRNG()
	:engines(), zeroToOne(0., 1.)
{
	int threads = std::max(1, omp_get_max_threads());
	for (int seed = 0; seed < threads; ++seed)
	{
		engines.push_back(RNGENGINE(seed));
	}
}


ThreadRNG::~ThreadRNG()
{
}

ThreadRNG& ThreadRNG::getInstance()
{
	static ThreadRNG singleThreadRNG;
	return singleThreadRNG;
}

double ThreadRNG::dRand()
{
	int tId = omp_get_thread_num();
	return zeroToOne(engines[tId]);
}

double ThreadRNG::dRand(double min, double max)
{
	int tId = omp_get_thread_num();
	return std::uniform_real_distribution<double>(min, max)(engines[tId]);
}
