#include "Plane.h"



Plane::Plane(glm::dvec3 centre, glm::dvec3 normal)
	: centre(centre), normal(glm::normalize(normal))
{
}


Plane::~Plane()
{
}

std::optional<Hit> Plane::intersect_plane(Ray ray) const
{
	// dist = dot(ro - center, normal)/dot(rd, normal)

	double denominator = glm::dot(ray.getDirection(), normal);
	if (abs(denominator) < 1e-6) { // ray is damn close to parallel, probably no hit
		return {};
	}

	double t0 = glm::dot(centre - ray.getOrigin(), normal) / denominator;
	if (t0 < 0.) {
		return {};
	}

	auto position = ray.getOrigin() + ray.getDirection() * t0;
	auto incident = ray.getDirection();
	auto hit_normal = glm::sign(denominator) < 0. ? normal : -normal; // flip normal to reflect back on the correct side

	return Hit(t0, position, incident, hit_normal, material.get());
}

std::optional<Hit> Plane::intersect(Ray ray) const
{
	return intersect_plane(ray);
}