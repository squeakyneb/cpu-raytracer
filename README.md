# CPU Raytracer

This is a CPU Raytracer. It renders images by tracing rays on a CPU. Currently, those images are of a monochrome lit sphere. It does not produce creative project names.

### Dependencies

* [TGAImage](https://danielbeard.wordpress.com/2011/06/06/image-saving-code-c/) (included in this repo as there is no apparent central distribution point for it)
* [argtable3](https://github.com/argtable/argtable3) (included as a git submodule)
* [OpenGL Mathematics](http://glm.g-truc.net/0.9.7/index.html) (included as a git submodule)

### How do I build this?

`git submodule update --init --recursive` if you haven't already to include submodules

Using Visual Studio 2022, the project should build as-is.

Using other compilers (only mildly tested):

 * `g++ -std=c++11 -Iinclude -fopenmp -O3 src/*.cpp` from the raycaster directory seems to work ok

### How to run it

`raycaster.exe <width> <height> <filename>`

### Contributions

This is kinda my own little project for my own achievements. Submitting issues in the bitbucket issue tracker is appreciated. If anyone has contributions that would make it compatible with non-Microsoft compilers (e.g. a makefile and some pre-processor magic) then I would accept such a pull request, as I have no intentions of doing this myself in the near future. However, I will not be accepting pull requests for features and such. Thanks anyway.

### License 

The following files are the product of Daniel Beard and have been loosely "licensed", under the terms of the statement "have fun using this in your projects". I will do exactly that, Daniel.

* `raycaster\include\Image.h`
* `raycaster\src\Image.cpp`

My code is licensed under the MIT License, as follows

Copyright (c) 2022 Benny Mackney

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.